package ru.neemble.mobiliser.importer.xml;

import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.Bank;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BankConsumer {

    public static List<Bank> getBankList(String fileName) {

        List<Bank> banks = new LinkedList<>();
        Map<String, List<Bank>> duplicate = new LinkedHashMap<>();

        try {

            File file = new File(fileName);
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document document = builder.parse(file);

            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("bank");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                String err = "";
                String key;

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    final Bank bank = new Bank();

                    NodeList idEl = eElement.getElementsByTagName("id");
                    long id = idEl == null || idEl.item(0) == null ? 0 : Long.parseLong(idEl.item(0).getTextContent());
                    System.out.print(" id: " + id);

                    NodeList nameRusEl = eElement.getElementsByTagName("nameRus");
                    String nameRus = nameRusEl == null || nameRusEl.item(0) == null ? null : nameRusEl.item(0).getTextContent();
                    System.out.print(" nameRus: " + nameRus);

                    NodeList nameEngEl = eElement.getElementsByTagName("nameEng");
                    String nameEng = nameEngEl == null || nameEngEl.item(0) == null ? null : nameEngEl.item(0).getTextContent();
                    System.out.print(" nameEng: " + nameEng);

                    NodeList bicEl = eElement.getElementsByTagName("bic");
                    String bic = bicEl == null || bicEl.item(0) == null ? null : bicEl.item(0).getTextContent();
                    System.out.print(" bic: " + bic);

                    NodeList corrAccountEl = eElement.getElementsByTagName("corrAccount");
                    String corrAccount = corrAccountEl == null || corrAccountEl.item(0) == null ? null : corrAccountEl.item(0).getTextContent();
                    System.out.println(" corrAccount: " + corrAccount);

                    if (id == 0) err += "(id == null)";
                    if (nameRus == null || nameRus.isEmpty()) err += "(nameRus == null)";
                    if (bic == null || bic.isEmpty()) err += "(bic == null)";
                    if (corrAccount == null || corrAccount.isEmpty()) err += "(corrAccount == null)";

                    if (err.isEmpty()) {
                        bank.setId(id);
                        bank.setNameRus(nameRus);
                        bank.setNameEng(nameEng);
                        bank.setBic(bic);
                        bank.setCorrAccount(corrAccount);
                        key = id + ":" + bic + ":" + corrAccount;
                        if (duplicate.containsKey(key)) {
                            System.out.println(temp + " duplicate : " + key);
                        } else {
                            duplicate.put(key, new LinkedList<Bank>() {{
                                add(bank);
                            }});
                            banks.add(bank);
                        }
                    } else {
                        System.out.println(temp + " : " + err);
                    }
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return banks;
    }
}
