package ru.neemble.mobiliser.importer;

import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.Atm;
import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.Bank;
import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.Office;

import java.util.Arrays;

public class Application {


    public static void main(String[] args) {
        System.out.println("INIT ..." + Arrays.toString(args));

        try {
            String file = null;
            String key = null;
            if (args[0].indexOf("-") == 0) {
                key = args[0];
                file = args[1];
            } else if (args[1].indexOf("-") == 0) {
                key = args[1];
                file = args[0];
            }
            assert key != null;
            switch (key) {
                case "-b":
                    CustomService.importFromFile(file, Bank.class);
                    break;
                case "-a":
                    CustomService.importFromFile(file, Atm.class);
                    break;
                case "-o":
                    CustomService.importFromFile(file, Office.class);
                    break;
                case "-t":
                    CustomService.test();
            }
        } catch (Exception e) {
            System.out.println("Error in parameters \nUsage: -a | b | o (atms, banks, offices) fileName\nExample: -b banks.xml");
        }

    }

}
