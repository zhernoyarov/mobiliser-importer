package ru.neemble.mobiliser.importer;

import com.google.gson.Gson;
import com.sybase365.mobiliser.custom.project.services.contract.v1_0.*;
import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.*;
import com.sybase365.mobiliser.framework.contract.v5_0.base.MobiliserRequestType;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import ru.neemble.mobiliser.importer.excel.AtmConsumer;
import ru.neemble.mobiliser.importer.excel.OfficeConsumer;
import ru.neemble.mobiliser.importer.xml.BankConsumer;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class CustomService {

    private static final Gson gson = new Gson();
    private static final String ORIGIN = "import";

    @SuppressWarnings("unchecked")
    public static <T> void importFromFile(String fileName, Class<T> type) {
        List<T> entities;
        if (type.isAssignableFrom(Bank.class)) {
            entities = (List<T>) BankConsumer.getBankList(fileName);
        } else if (type.isAssignableFrom(Office.class)) {
            entities = (List<T>) OfficeConsumer.getOfficeList(fileName);
        } else if (type.isAssignableFrom(Atm.class)) {
            entities = (List<T>) AtmConsumer.getAtmList(fileName);
        } else {
            System.out.println("Unrecognised");
            return;
        }

        int partitionSize = 100;
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        System.out.println("SEND...");
        long startTotal = System.currentTimeMillis();
        for (int i = 0; i < entities.size(); i += partitionSize) {
            int size = Math.min(i + partitionSize, entities.size());
            sendPart(entities.subList(i, size), type);
        }
        long endTotal = System.currentTimeMillis();
        System.out.println("DONE \nTOTAL TIME " + sdf.format(new Date(endTotal - startTotal)));
    }

    @SuppressWarnings("unchecked")
    public static <T> void sendPart(List<T> part, Class<T> type) {
        HttpPost httpPost = null;
        System.out.println("SEND PART");
        Object request = null;
        if (type.isAssignableFrom(Bank.class)) {
            httpPost = new HttpPost("/mobiliser/rest/custom/updateRibBankList");
            request = new UpdateRibBankListRequest();
            ((UpdateRibBankListRequest) request).getBank().addAll((List<Bank>) part);
        } else if (type.isAssignableFrom(Office.class)) {
            httpPost = new HttpPost("/mobiliser/rest/custom/updateRibOfficeList");
            request = new UpdateRibOfficeListRequest();
            ((UpdateRibOfficeListRequest) request).getOffice().addAll((List<Office>) part);
        } else if (type.isAssignableFrom(Atm.class)) {
            httpPost = new HttpPost("/mobiliser/rest/custom/updateRibAtmList");
            request = new UpdateRibAtmListRequest();
            ((UpdateRibAtmListRequest) request).getAtm().addAll((List<Atm>) part);
        }

        if (request == null) return;
        ((MobiliserRequestType) request).setOrigin(ORIGIN);
        ((MobiliserRequestType) request).setTraceNo(UUID.randomUUID().toString());

        String requestJson = gson.toJson(request);

        StringEntity requestEntity;
        try {
            requestEntity = new StringEntity(requestJson, "UTF-8");
            httpPost.setEntity(requestEntity);
            HttpResponse response = ServiceClient.execute(httpPost);

            int statusCode = response == null ? -1 : response.getStatusLine().getStatusCode();
            System.out.println("STATUS: " + statusCode + " ");
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                String apiOut = EntityUtils.toString(entity, Charset.forName("UTF-8"));
                if (type.isAssignableFrom(Bank.class)) {
                    GetRibBankListResponse obj = gson.fromJson(apiOut, GetRibBankListResponse.class);
                    System.out.println(" UPDATED " + obj.getBank().size() + " ROWS ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void test() {
        HttpPost httpPost = new HttpPost("/mobiliser/rest/custom/createRibTaxPayerStatusCode");

        TaxPayerStatusCode request1Entity = new TaxPayerStatusCode();
        request1Entity.setCode("0012.001");
        request1Entity.setDescription("woooozzz!");
        request1Entity.setId(0);

        CreateRibTaxPayerStatusCodeRequest request = new CreateRibTaxPayerStatusCodeRequest();
        request.setTaxPayerStatusCode(request1Entity);
        request.setTraceNo(UUID.randomUUID().toString());
        request.setOrigin(ORIGIN);
        String requestJson = gson.toJson(request);

        StringEntity requestEntity;
        try {
            requestEntity = new StringEntity(requestJson, "UTF-8");
            httpPost.setEntity(requestEntity);
            HttpResponse response = ServiceClient.execute(httpPost);

            int statusCode = response == null ? -1 : response.getStatusLine().getStatusCode();
            System.out.println("STATUS: " + statusCode + " ");

            if (statusCode == 200) {

                HttpEntity responseEntity = response.getEntity();
                String responseJson = EntityUtils.toString(responseEntity, Charset.forName("UTF-8"));
                CreateRibTaxPayerStatusCodeResponse obj = gson.fromJson(responseJson, CreateRibTaxPayerStatusCodeResponse.class);
                String responseCode = obj.getTaxPayerStatusCode().getCode();
                System.out.println(" ADDED " + responseCode);

                if (responseCode != null && !responseCode.isEmpty()) {
                    httpPost = new HttpPost("/mobiliser/rest/custom/getRibTaxPayerStatusCodeDescription");
                    GetRibTaxPayerStatusCodeDescriptionRequest request2 = new GetRibTaxPayerStatusCodeDescriptionRequest();
                    request2.setCode(responseCode);
                    requestJson = gson.toJson(request2);
                    requestEntity = new StringEntity(requestJson, "UTF-8");
                    httpPost.setEntity(requestEntity);
                    response = ServiceClient.execute(httpPost);

                    statusCode = response == null ? -1 : response.getStatusLine().getStatusCode();
                    System.out.println("STATUS: " + statusCode + " ");
                    if (statusCode == 200) {
                        responseEntity = response.getEntity();
                        responseJson = EntityUtils.toString(responseEntity, Charset.forName("UTF-8"));
                        System.out.println("!!!\n " + responseJson);
                        GetRibTaxPayerStatusCodeDescriptionResponse obj2 = gson.fromJson(responseJson, GetRibTaxPayerStatusCodeDescriptionResponse.class);
                        System.out.println("FOUND " + obj2.getDescription());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
