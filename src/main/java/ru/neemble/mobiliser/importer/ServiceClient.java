package ru.neemble.mobiliser.importer;

import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

public class ServiceClient {
    private static final HttpHost httpHost = new HttpHost("localhost", 8080, "http");
    private static final CloseableHttpClient httpClient = HttpClients.createDefault();
    private static final CredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider();
    private static final HttpClientContext context = HttpClientContext.create();
    private static final AuthCache authCache = new BasicAuthCache();
    private static final BasicScheme basicScheme = new BasicScheme();
    private static boolean isReady = false;

    private static void init() {
        basicCredentialsProvider.setCredentials(
                new AuthScope(httpHost.getHostName(), httpHost.getPort()),
                new UsernamePasswordCredentials("mobiliser", "secret")
        );
        authCache.put(httpHost, basicScheme);
        context.setCredentialsProvider(basicCredentialsProvider);
        context.setAuthCache(authCache);
        isReady = true;
    }

    public static HttpResponse execute(HttpRequest request) {
        try {
            if (!isReady) init();
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-Type", "application/json; charset=utf-8");
            return httpClient.execute(httpHost, request, context);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
