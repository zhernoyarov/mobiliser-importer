package ru.neemble.mobiliser.importer.excel;

import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.Office;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OfficeConsumer {

    public static List<Office> getOfficeList(String fileName) {
        List<Office> officeList = new LinkedList<>();
        try {

            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;

            int rows = sheet.getPhysicalNumberOfRows();

            int cols = 0;
            int tmp;

            for (int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if (row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if (tmp > cols) cols = tmp;
                }
            }

            System.out.println("rows = " + rows + " cols = " + cols);

            Map<String, Office> errors = new HashMap<>();
            Map<String, List<Office>> duplicate = new LinkedHashMap<>();

            for (int r = 1; r < rows; r++) {
                row = sheet.getRow(r);
                if (row != null) {

                    final Office office = new Office();

                    Double d = row.getCell(0).getNumericCellValue();
                    office.setId(d.longValue());
                    office.setPcDivisionCode(row.getCell(1).getStringCellValue());
                    office.setNameRus(row.getCell(2).getStringCellValue());
                    office.setAddressRus(row.getCell(3).getStringCellValue());
                    office.setPhone(row.getCell(4).getStringCellValue());
                    boolean f = true;
                    while (r + 1 < rows && f) {
                        HSSFRow subRow = sheet.getRow(r + 1);
                        String nextPhone = subRow.getCell(4).getStringCellValue();
                        if (subRow.getCell(0).getNumericCellValue() == 0 && nextPhone != null && !nextPhone.isEmpty()) {
                            office.setPhone(office.getPhone() + ", " + nextPhone);
                            r++;
                        } else {
                            f = false;
                        }
                    }
                    if (row.getCell(5) != null) {
                        office.setMetro(row.getCell(5).getStringCellValue());
                    }

                    office.setNameEng(row.getCell(6).getStringCellValue());
                    office.setAddressEng(row.getCell(7).getStringCellValue());
                    String lon = row.getCell(9).getStringCellValue();
                    String lat = row.getCell(8).getStringCellValue();
                    Pattern regex = Pattern.compile("(?<=\\()([^)]*)(?=\\))");
                    Matcher matcher = regex.matcher(lon);
                    if (matcher.find()) office.setLongitude(Double.parseDouble(matcher.group()));
                    matcher = regex.matcher(lat);
                    if (matcher.find()) office.setLatitude(Double.parseDouble(matcher.group()));

                    office.setWorkingHours(row.getCell(10).getStringCellValue());

                    String err = r + "; ";
                    String key = r + ":";
                    if (office.getId() == 0) {
                        err += "ID = 0;";
                    } else {
                        key += office.getId();
                    }

                    if (office.getNameRus() == null || office.getNameRus().isEmpty()) {
                        err += "NameRus is null;" + office.getNameRus();
                    } else {
                        key += office.getAddressRus() + ":";
                    }
                    if (office.getAddressRus() == null || office.getAddressRus().isEmpty()) {
                        err += "AddressRus is null;" + office.getAddressRus();
                    } else {
                        key += office.getAddressRus();
                    }

                    if (!err.isEmpty() && (err.contains("ID") || err.contains("NameRus") || err.contains("AddressRus"))) {
                        errors.put(err, office);
                    } else {
                        if (duplicate.containsKey(key)) {
                            List<Office> list = duplicate.get(key);
                            list.add(office);
                            duplicate.put(key, list);
                        } else {
                            duplicate.put(key, new LinkedList<Office>() {{
                                add(office);
                            }});
                            officeList.add(office);
                        }
                    }
                }
            }

            System.out.println("ERRORS: " + errors.size());
            for (Map.Entry<String, Office> entry : errors.entrySet()) {
                System.out.println(entry.getKey());
            }
            int count = 0;
            for (Map.Entry<String, List<Office>> entry : duplicate.entrySet()) {
                if (entry.getValue().size() > 1) {
                    count++;
                    System.out.println(entry.getKey());
                    for (Office office : entry.getValue()) {
                        System.out.println("|---" + office.getId() + ", " + office.getNameRus() + ", " + office.getAddressRus());
                    }
                }
            }
            System.out.println("DUPLICATE: " + count);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return officeList;
    }

}
