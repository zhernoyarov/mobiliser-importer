package ru.neemble.mobiliser.importer.excel;

import com.sybase365.mobiliser.custom.project.services.contract.v1_0.beans.Atm;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class AtmConsumer {

    public static List<Atm> getAtmList(String fileName) {
        List<Atm> atmList = new LinkedList<>();
        try {

            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;

            int rows = sheet.getPhysicalNumberOfRows();

            int cols = 0;
            int tmp;

            for (int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if (row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if (tmp > cols) cols = tmp;
                }
            }

            System.out.println("rows = " + rows + " cols = " + cols);


            Map<String, Atm> errors = new HashMap<>();
            Map<String, List<Atm>> duplicate = new LinkedHashMap<>();

            for (int r = 1; r < rows; r++) {
                row = sheet.getRow(r);
                if (row != null && row.getCell(2).getStringCellValue() != null
                        && !row.getCell(2).getStringCellValue().isEmpty()
                        && row.getCell(2).getStringCellValue().toUpperCase().contains("БАНКОМАТ")) {
                    final Atm atm = new Atm();

                    atm.setPointId(row.getCell(0).getStringCellValue());
                    atm.setBankName(row.getCell(1).getStringCellValue());

                    List<String> operationList = new ArrayList<>(Arrays.asList(row.getCell(3).getStringCellValue().toLowerCase().split(",")));
                    String[] currencyList = row.getCell(4).getStringCellValue().split(",");

                    atm.setCashIn(operationList.contains("cash-in"));
                    atm.setCashOut(operationList.contains("cash-out"));
                    atm.setCurrency1(currencyList.length > 0 && currencyList[0] != null ? currencyList[0] : "");
                    atm.setCurrency2(currencyList.length > 1 && currencyList[1] != null ? currencyList[1] : "");
                    atm.setCurrency3(currencyList.length > 2 && currencyList[2] != null ? currencyList[2] : "");
                    atm.setCurrency4(currencyList.length > 3 && currencyList[3] != null ? currencyList[3] : "");
                    atm.setAddBuilding(row.getCell(17).getStringCellValue());
                    atm.setSchedule(row.getCell(18).getStringCellValue());
                    atm.setAccess(row.getCell(19).getStringCellValue());
                    atm.setMetro(row.getCell(20).getStringCellValue());

                    String cellLong = row.getCell(21).getStringCellValue();
                    String cellLat = row.getCell(22).getStringCellValue();

                    String longitude = cellLong.replace(",", "").trim().replaceAll("\u00A0", "");
                    String latitude = cellLat.replace(",", "").trim().replaceAll("\u00A0", "");
                    if (!latitude.isEmpty()) atm.setLatitude(Double.valueOf(latitude));
                    if (!longitude.isEmpty()) atm.setLongitude(Double.valueOf(longitude));

                    atm.setAtmNetworkName("ОРС");
                    atm.setPointType(row.getCell(2).getStringCellValue());


                    String address = "";
                    String subjectType = row.getCell(5).getStringCellValue();
                    String subjectName = row.getCell(6).getStringCellValue();
                    String s = subjectType.toUpperCase();
                    switch (s) {
                        case "Г":
                        case "РЕСП":
                            address += subjectType + ". " + subjectName;
                            break;
                        case "AO":
                            address += subjectName;
                            break;
                        default:
                            address += subjectName + " " + subjectType;
                            if (s.equals("ОБЛ")) {
                                address += ".";
                            }
                            break;
                    }
                    String subSubjectType = row.getCell(7).getStringCellValue();
                    String subSubjectName = row.getCell(8).getStringCellValue();
                    if (!subjectType.isEmpty() && !subSubjectName.isEmpty())
                        address += ", " + subjectName + " " + subSubjectType + ".";

                    String settlementType = row.getCell(9).getStringCellValue();
                    String settlementName = row.getCell(10).getStringCellValue();

                    if (!settlementName.isEmpty()) {
                        address += ", ";
                        if (!settlementType.isEmpty()) address += settlementType + ". ";
                        address += settlementName;
                    }

                    String buildingType = row.getCell(11).getStringCellValue();
                    String buildingName = row.getCell(12).getStringCellValue();

                    s = buildingType.toUpperCase();
                    if (!buildingName.isEmpty()) {
                        if (!buildingType.isEmpty() && buildingName.contains(buildingType)) {
                            buildingName = buildingName.replace(buildingType, "");
                        }
                        address += ", ";

                        switch (s) {
                            case "АУЛ":
                            case "САД":
                            case "СЕЛО":
                                address += buildingType + " " + buildingName;
                                break;
                            case "А/Я":
                            case "Д":
                            case "УЛ":
                            case "П":
                            case "ПЛ":
                            case "ПС":
                            case "П/О":
                            case "П/СТ":
                            case "С":
                            case "СТ":
                            case "ПЕР":
                                address += buildingType + ". " + buildingName;
                                break;
                            case "ПР-КТ":
                                address += buildingName.endsWith("ИЙ")
                                        ? buildingName + " " + buildingType + "."
                                        : buildingType + ". " + buildingName;
                                break;
                            case "НАБ": {
                                address += buildingName.split(" ").length > 1
                                        ? buildingType + ". " + buildingName
                                        : buildingName + " " + buildingType + ". ";
                                break;
                            }
                            case "КОЛЬЦО":
                            case "НП":
                            case "ТЕР":
                                address += buildingName;
                                break;
                            case "ЛИНИЯ": {
                                String[] sa = buildingName.split(" ");
                                address += sa.length > 1
                                        ? sa[1].equals(buildingType)
                                        ? buildingName
                                        : sa[0] + " " + buildingType + " " + sa[1]
                                        : buildingName + " " + buildingType;
                                break;
                            }
                            default:
                                address += buildingName + " " + (buildingType.isEmpty() ? "" : buildingType + ".");
                                break;
                        }
                    }
                    String house = row.getCell(13).getStringCellValue();
                    if (!house.isEmpty()) address += ", д. " + house;

                    String corp = row.getCell(14).getStringCellValue();
                    if (!corp.isEmpty()) address += ", к. " + corp;

                    String building = row.getCell(15).getStringCellValue();
                    if (!building.isEmpty()) address += ", ст. " + building;

                    String letter = row.getCell(16).getStringCellValue();
                    if (!letter.isEmpty()) address += ", лит. " + letter;

                    atm.setAddress(address);

                    String err = "row: " + r + " ";
                    String pointId = atm.getPointId();

                    if (pointId == null || pointId.isEmpty()) err += "ID = '" + pointId + "'";

                    String bankNameRu = atm.getBankName();
                    if (bankNameRu == null || bankNameRu.isEmpty()) err += "BankNameRu '" + bankNameRu + "'";

                    if (longitude.isEmpty() || latitude.isEmpty())
                        err += "LAT - LON '" + cellLat + "' - '" + cellLong + "'";

                    if (Arrays.asList(atm.getPointType().toUpperCase().split(",")).contains("БАНКОМАТ")) {
                        if (pointId == null || pointId.isEmpty() || bankNameRu == null || bankNameRu.isEmpty()
                                || longitude.isEmpty() || latitude.isEmpty())
                            errors.put(err, atm);
                        else {
                            String key = pointId + ":" + bankNameRu + ":" + address;
                            if (duplicate.containsKey(key)) {
                                List<Atm> list = duplicate.get(key);
                                list.add(atm);
                                duplicate.put(key, list);
                            } else {
                                atmList.add(atm);
                                duplicate.put(key, new LinkedList<Atm>() {{
                                    add(atm);
                                }});
                            }
                        }
                    }
                }
            }

            System.out.println("ERRORS: " + errors.size());
            for (Map.Entry<String, Atm> entry : errors.entrySet()) {
                Atm atm = entry.getValue();
                System.out.println(
                        entry.getKey() + " " +
                                atm.getId() +
                                " " + atm.getBankName() +
                                " LN : '" + atm.getLongitude() + "'" +
                                " LT : '" + atm.getLatitude() + "'");
            }
            int dCount = 0;
            for (Map.Entry<String, List<Atm>> entry : duplicate.entrySet()) {
                if (entry.getValue().size() > 1) {
                    System.out.println(entry.getKey());
                    for (Atm atm : entry.getValue()) {
                        System.out.println(
                                " |---" +
                                        atm.getPointId() +
                                        " " + atm.getBankName() +
                                        " " + atm.getAddress() +
                                        " LN : '" + atm.getLongitude() + "'" +
                                        " LT : '" + atm.getLatitude() + "'");
                    }
                    dCount++;
                }
            }
            System.out.println("DUPLICATE: " + dCount);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return atmList;
    }

}
